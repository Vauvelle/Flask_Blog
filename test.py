from webapp import create_app
import unittest

app = create_app()

class FlaskTestCase(unittest.TestCase):

    # Ensure that flask was set up correctly
    def test_index(self):
        tester = app.test_client(self)
        response = tester.get('/login', content_type='html/text')
        self.assertEqual(response.status_code, 200)

    # Ensure that the login page load correctly
    def test_login_page_loads(self):
        tester = app.test_client(self)
        response = tester.get('/login', content_type='html/text')
        self.assertTrue(b'Log In' in response.data)

    # Ensure that the login behaves correctly given correct credentials
    # Ensure that the login behaves incorrectly given incorrect credentls
    # Ensure that the logout behaves correctly

if __name__ == '__main__':
    unittest.main()